import pandas as pd
from PIL import Image
import numpy as np
import os
import cv2
import matplotlib.pyplot as plt

def reshape_to_origin(im_pth:str):
    desired_size = 237

    im = cv2.imread(im_pth)
    old_size = im.shape[:2] # old_size is in (height, width) format
    # cv2.imshow("old", im)
    ratio = float(desired_size)/max(old_size)
    new_size = tuple([int(x*ratio) for x in old_size])

    # new_size should be in (width, height) format

    im = cv2.resize(im, (new_size[1], new_size[0]))

    delta_w = desired_size - new_size[1]
    delta_h = desired_size - new_size[0]
    top, bottom = delta_h//2, delta_h-(delta_h//2)
    left, right = delta_w//2, delta_w-(delta_w//2)

    color = [255, 255, 255]
    new_im = cv2.copyMakeBorder(im, top, bottom, left, right, cv2.BORDER_CONSTANT,
        value=color)

    new_im = cv2.resize(im, (237, 137))
    # cv2.imshow("new", new_im[:, :, 2])
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    return new_im

# path = "MatriVasha_raw/feamale/118/1_BAR_13_1_401.jpg"
# img = reshape_to_origin(path)
# # img = Image.open(path)
# # print(img.info)
# # plt.waitforbuttonpress(0)
# img_converted = np.array(img)
# if len(img_converted.shape) > 2:
#     img_converted = img_converted[:, :, 0]
# row = img_converted.ravel()
# row_as_list = row.tolist()
# print(img_converted.shape)
# print(len(row_as_list))

data = []
image_id = []
i = 0
for k in range(60):
    for current_dir, dirs, files in os.walk("MatriVasha_raw/feamale/"+str(k)):
        for f in files:
            # if current_dir == "MatriVasha_raw.DS_Store":
            # 	print(current_dir)
            if f != ".DS_Store":
                path = os.path.join(current_dir, f)
                print("PROCESSING "+str(i)+" :", path)
                img = reshape_to_origin(path)
                img_converted = np.array(img)
                if len(img_converted.shape) > 2:
                    img_converted = img_converted[:, :, 0]
                print(img_converted.shape)
                row = img_converted.ravel()
                row_as_list = row.tolist()
                data.append(row_as_list)
                image_id.append("Test_" + str(i))
                i += 1
print("PROCESSING COMPLETE!")
print("MAKING DATAFRAME")
df = pd.DataFrame(data)
df.insert(0, "image_id", image_id)
print("CONVERTING TO CSV")
# df.columns = df.columns.astype(str)
df.to_csv("matrivasha_feamale1.csv", index=False)

df1 = pd.read_csv("matrivasha_feamale1.csv")
print(df1.head())


# test = test.resize((237, 136))
# print(list(test.getdata()))
# data = []
# image_id = []
# pathes = ["MatriVasha_raw/feamale/0/1_BAG_13_1_1100.jpg",
# 	"MatriVasha_raw/feamale/0/1_BAR_11_1_767.jpg",
# 	"MatriVasha_raw/feamale/0/1_BAR_15_1_477.jpg"]
# i = 0
# for path in pathes:
# 	img = Image.open(path)
# 	img = img.resize((237, 136))
# 	img_converted = np.array(img)
# 	row =  img_converted.ravel()
# 	row_as_list = row.tolist()
# 	data.append(row_as_list)
# 	image_id.append("Train_"+str(i))
# 	i += 1

# df = pd.DataFrame(data)
# df.insert(0, "image_id", image_id)
# df.columns = df.columns.astype(str)
# df.to_parquet('example.parquet')
# df = pd.read_parquet('example.parquet')
# print(df.head())
