import argparse
import os
import sys
import gc
from importlib import import_module

import pandas as pd
import numpy as np

LOCAL = False
if not LOCAL:
    sys.path.append("../input/newdata2")

from bengaliai.data_utils import read_parquets
from bengaliai.util import (
    convert_to_categorical,
    make_submission,
    instance_anything,
    debug_parquet_multiplier,
    rescore_pred_matrix,
)
from bengaliai.inference_model import InferenceNet

import torch


def kaggle_path_changer(path):
    name = "_".join(path.split("/")[-2:])
    return os.path.join("../input/weights", name)


def load_model(model, path_to_checkpoint):
    checkpoint = torch.load(path_to_checkpoint, map_location=lambda storage, loc: storage)
    model.load_state_dict(checkpoint["state_dict"])
    model.to("cuda")
    return model


if __name__ == "__main__":

    if LOCAL:
        parquets = (
            "/data/test_image_data_0.parquet",
            "/data/test_image_data_1.parquet",
            "/data/test_image_data_2.parquet",
            "/data/test_image_data_3.parquet",
        )

    else:
        parquets = (
            "../input/bengaliai-cv19/test_image_data_0.parquet",
            "../input/bengaliai-cv19/test_image_data_1.parquet",
            "../input/bengaliai-cv19/test_image_data_2.parquet",
            "../input/bengaliai-cv19/test_image_data_3.parquet",
        )

    parser = argparse.ArgumentParser(description="run config")
    parser.add_argument("--config", type=str, help="path to module", default="predict_config")
    if not LOCAL:
        config_module = "predict_config"
    else:
        args = parser.parse_args()
        config_module = args.config

    config_module = import_module(config_module)  # load config as python module
    configs = config_module.configs
    data_config = config_module.data_config
    # df = pd.read_csv(config["data"]["test_df_path"], encoding="utf")

    # parquets = debug_parquet_multiplier(parquets)

    preds = []
    for parquet_path in parquets:
        data_config["stages"][0]["test"]["dataset_constructor"]["kwargs"]["parquet_path"] = [parquet_path]
        config_data = data_config["stages"][0]["test"]
        dataset = instance_anything(config_data["dataset_constructor"])
        config_data["dataloader_constructor"]["kwargs"]["dataset"] = dataset
        dataloader = instance_anything(config_data["dataloader_constructor"])
        res = None
        for config in configs:
            model = InferenceNet(instance_anything(config["backbone"]))
            path = config["path_to_checkpoint"]
            if not LOCAL:
                path = kaggle_path_changer(path)
            model = load_model(model, path)

            if res is None:
                res = model.predict(dataloader)
            else:
                res += model.predict(dataloader)

        res = convert_to_categorical(res)
        preds.append(res)

    final_pred = np.vstack(preds)
    make_submission(final_pred)
