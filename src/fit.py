import argparse
import os
from importlib import import_module

import pandas as pd
from sklearn.model_selection import KFold

from bengaliai.data_utils import read_parquets
from bengaliai.config_utils import add_file_hash_and_save, setup_lightning_callbacks_and_loggers
from bengaliai.make_ohe import Ohe

# from bengaliai.cleanlab_noise import NoiseIndicator
from bengaliai.custom_validation import CustomValidation
from bengaliai.util import instance_anything

import torch
from iterstrat.ml_stratifiers import MultilabelStratifiedKFold
from pytorch_lightning import Trainer

import numpy as np


def set_data_in_model_config(model_config, data_config, stage=0):
    model_config["stages"][stage]["train"]["dataset_constructor"]["kwargs"]["parquet_path"] = data_config["data"][
        "train_parquets"
    ]
    model_config["stages"][stage]["val"]["dataset_constructor"]["kwargs"]["parquet_path"] = data_config["data"][
        "train_parquets"
    ]

    model_config["stages"][stage]["train"]["dataset_constructor"]["kwargs"]["target_path"] = data_config["data"][
        "train_df_path"
    ]
    model_config["stages"][stage]["val"]["dataset_constructor"]["kwargs"]["target_path"] = data_config["data"][
        "train_df_path"
    ]


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="run config")
    parser.add_argument("--config", type=str, help="path to module", default="config")
    parser.add_argument("--hash", action="store_true", help="path to module")
    parser.add_argument(
        "--noise",
        action="store_true",
        help="cleanlab search for noise \
                                    elements, DO NOT USE WITH DEBUG IN CONFIG ENABLED!",
    )

    args = parser.parse_args()
    config_module = import_module(args.config)  # load config as python module
    model_config = config_module.model_config
    training_config = config_module.training_config
    data_config = config_module.data_config

    if args.hash:
        training_config = add_file_hash_and_save(args.config, training_config)

    if args.noise:
        noise = NoiseIndicator()

    df = pd.read_csv(data_config["data"]["train_df_path"], encoding="utf")
    print(df.shape)

    df = df.iloc[:, :4]
    y = df.iloc[:, 1:]  # use only for statification
    cv = instance_anything(training_config["cv"])
    if training_config["pseudo"] is True:
        df, pseudolables_df = df.iloc[:200840, :], df.iloc[200840:, :]
        y, y_df = y.iloc[:200840, :], y.iloc[200840:, :]
    # if True:
    #     ohe = Ohe("/data/class_map.csv", all_comb=True)
    #     uniq = ohe.get_uniq_labels(y.values)
    #     np.save("all_uniq_triplets", uniq)
    #     print(uniq)
    #     1/0
    set_data_in_model_config(model_config, data_config, stage=0)

    if "debug" in data_config:
        num_samples = data_config["debug"]["df_num_samples"]
        df, y = df.iloc[:num_samples], y.iloc[:num_samples]

    folds = cv.split(df, y)
    for fold in training_config["cv"]["using_splits"]:
        train_index, val_index = folds[0][fold], folds[1][fold]

        if training_config["pseudo"] is True:  # TODO
            train_index = np.hstack((train_index, list(range(200840, 507301))))

        # print(train_index, val_index)
        # print(train_index.shape, val_index.shape)
        # 1/0

        name = f"{training_config['name']}_fold_{fold}"
        print(f"Now we are training - {name}")

        training_config = setup_lightning_callbacks_and_loggers(training_config, name)

        model_config["stages"][0]["train"]["dataset_constructor"]["kwargs"]["using_idxs"] = train_index
        # model_config["stages"][0]["test"]["dataset_constructor"]["kwargs"]["using_idxs"] = val_index  # kostyl?
        model_config["stages"][0]["val"]["dataset_constructor"]["kwargs"]["using_idxs"] = val_index

        model = model_config["base_model"](model_config)

        trainer = Trainer(**training_config["trainer"])
        # path_to_checkpoint = "/src/workspace/models/se_resnext_50_lightaug_cutmixdoubleloss_cos_sgd_labelsmooth-2440567894052788076_fold_2/_ckpt_epoch_49.ckpt"
        # ckpt = torch.load(path_to_checkpoint, map_location=lambda storage, loc: storage)
        # trainer.restore(path_to_checkpoint, True)
        # print(dir(model))
        # model.load_from_checkpoint("/src/workspace/models/se_resnext_50_lightaug_cutmixdoubleloss_cos_sgd_labelsmooth-2440567894052788076_fold_2/_ckpt_epoch_49.ckpt")
        # 1/0
        trainer.fit(model)
        if args.noise:
            noise.extend_labels(model.get_truth_matrix())
            noise.extend_proba(model.get_predict_matrix())

    if args.noise:
        noisy_indices = noise.get_noisy_indices()
        np.save("../playground/noise_indices", noisy_indices)
