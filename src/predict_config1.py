import os

from bengaliai.dataset import BengaliaiDataset
from bengaliai.dataset import MatriVasha
from bengaliai.transforms import OneToThree
from bengaliai.callbacks import CleanupCallback, ComputeValLoss, PredictCallback
from bengaliai.backbones import get_model, get_resnet_34, get_seresnext50

import torch
from torch.optim.lr_scheduler import ReduceLROnPlateau
from torch.optim import Adam
from torch.utils.data import DataLoader
import numpy as np
from albumentations import (
    CLAHE,
    RandomRotate90,
    Transpose,
    ShiftScaleRotate,
    Blur,
    OpticalDistortion,
    GridDistortion,
    Compose,
    Resize,
    Normalize,
    HorizontalFlip,
    RandomBrightnessContrast,
    RandomResizedCrop,
    VerticalFlip,
    Rotate,
    CenterCrop,
)
from albumentations.pytorch import ToTensorV2


BATCH_SIZE = 380
LOCAL = False

checkpoint_folder = "/src/workspace/models/seresnext_50_adam_kfold_gridmask_from-6781659589419745391_fold_0/"
checkpoint_num = "_ckpt_epoch_163.ckpt"
path_to_checkpoint = os.path.join("../input/weights", checkpoint_num)
path_to_local_checkpoint = os.path.join(checkpoint_folder, checkpoint_num)

augs = Compose([Resize(224, 224), OneToThree(), ToTensorV2()])
# augs1 = Compose([CenterCrop(236, 137) , Resize(224, 224), OneToThree(), ToTensorV2()]) #изначальный размер картинки 148х147, не думаю, что такой crop будет корректен

config = {
    "data": {"test_df_path": "../input/bengaliai-cv19/test.csv"},
    "backbone": {"base_class": get_seresnext50, "kwargs": {"out": 186, "pretrained": None}},
    "path_to_checkpoint": path_to_checkpoint,
    "stages": [
        # 0 stage
        {
            "test": {
                "dataset_constructor": {"base_class": BengaliaiDataset, "kwargs": {"labels": False, "transform": augs}},
                "dataloader_constructor": {
                    "base_class": DataLoader,
                    "kwargs": {"num_workers": 2, "pin_memory": True, "batch_size": BATCH_SIZE, "shuffle": False},
                },
            }
        },
        # 1 stage
        {
            "test": {
                "dataset_constructor": {"base_class": MatriVasha, "kwargs": {"labels": False, "transform": augs},},
                "dataloader_constructor": {
                    "base_class": DataLoader,
                    "kwargs": {"num_workers": 2, "pin_memory": True, "batch_size": BATCH_SIZE, "shuffle": False},
                },
            }
        },
    ],
}

if LOCAL:
    parquets = (
        "/data/test_image_data_0.parquet",
        "/data/test_image_data_1.parquet",
        "/data/test_image_data_2.parquet",
        "/data/test_image_data_3.parquet",
    )

else:
    parquets = (
        "../input/bengaliai-cv19/test_image_data_0.parquet",
        "../input/bengaliai-cv19/test_image_data_1.parquet",
        "../input/bengaliai-cv19/test_image_data_2.parquet",
        "../input/bengaliai-cv19/test_image_data_3.parquet",
    )

config["test_parquets"] = parquets
