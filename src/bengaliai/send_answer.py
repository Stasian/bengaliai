import os
import json
import time


def send_answer(
    name_id,
    code_file,
    path_to_files,
    path_to_weights,
    user="stasian",
    language="python",
    kernel_type="script",
    is_private="true",
    enable_gpu="true",
    enable_internet="false",
    competition_sources=["bengaliai-cv19"],
    kernel_sources=[],
):
    """
    This function sends code_file and any else file in kaggle. For use you have to do pip install kaggle

    Example:

    send_answer(name_id="{user}/kernel3d4566be37", code_file='main.py', enable_gpu='false', path_to_files="/home/kagglework/")

    name_id: it's id your kernel. Can you get it here https://www.kaggle.com/kernels/welcome?competitionId=14897
    code_file: it's file name with script
    path_to_files: the path in which all the date is
    Read more about metadata https://github.com/Kaggle/kaggle-api/wiki/Kernel-Metadata
    """
    print("id", f"{user}/{name_id}")
    current_time = time.ctime()
    title = name_id
    metadata_dataset = {"title": "new_data2", "id": f"{user}/newdata2", "licenses": [{"name": "CC0-1.0"}]}
    with open(os.path.join(path_to_files, "dataset-metadata.json"), "w") as f:
        f.write(json.dumps(metadata_dataset, indent=4))
    comand_create_dataset = f'kaggle datasets version -p {path_to_files} --dir-mode zip  -m "{current_time}"'
    os.system(comand_create_dataset)

    metadata_dataset_weights = {"title": "new_data_weights", "id": f"{user}/weights", "licenses": [{"name": "CC0-1.0"}]}
    with open(os.path.join(path_to_weights, "dataset-metadata.json"), "w") as f:
        f.write(json.dumps(metadata_dataset_weights, indent=4))
    comand_create_dataset_weights = f'kaggle datasets version -p {path_to_weights} --dir-mode zip -m "{current_time}"'
    os.system(comand_create_dataset_weights)

    dataset_sources = [f"{user}/weights", f"{user}/newdata2", "shutil/nvidia-apex", "stasian/nvidiaapex"]
    metadata_kernel = {
        "id": f"{user}/{name_id}",
        "title": title,
        "code_file": code_file,
        "language": language,
        "kernel_type": kernel_type,
        "is_private": is_private,
        "enable_gpu": enable_gpu,
        "enable_internet": enable_internet,
        "dataset_sources": dataset_sources,
        "competition_sources": competition_sources,
        "kernel_sources": kernel_sources,
    }
    with open(os.path.join(path_to_files, "kernel-metadata.json"), "w") as f:
        f.write(json.dumps(metadata_kernel, indent=4))
    push_kernel = f"kaggle kernels push -p {path_to_files}"
    time.sleep(120)
    os.system(push_kernel)
