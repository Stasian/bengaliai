import numpy as np
import pandas as pd

import numpy as np
import pandas as pd


class Ohe:
    """Makes onehot from an array"""

    def __init__(self, class_mapping_path: str, all_comb=True):
        self.all_combinations = all_comb
        self.df = pd.read_csv(class_mapping_path)
        self.grapheme_len = self.df[self.df["component_type"] == "grapheme_root"]["label"].unique().size
        self.vowel_len = self.df[self.df["component_type"] == "vowel_diacritic"]["label"].unique().size
        self.consonant_len = self.df[self.df["component_type"] == "consonant_diacritic"]["label"].unique().size

    def __call__(self, arr: np.array) -> np.array:
        return self._create_ohe(arr)

    def _create_ohe(self, arr: np.array) -> np.array:
        labels_amount = self.grapheme_len * self.vowel_len * self.consonant_len
        ohe = np.zeros((arr.shape[0], labels_amount), dtype="int16")
        columns = np.array([], dtype="int16")
        for idx, sample in enumerate(arr):
            column = sample[0] * self.vowel_len * self.consonant_len + sample[1] * self.consonant_len + sample[2]
            if column not in columns:
                columns = np.append(columns, column)
            ohe[idx, column] = 1
        if not self.all_combinations:
            return ohe[:, columns]
        return ohe

    def encode_label(self, sample: np.array) -> int:
        try:
            return sample[0] * self.vowel_len * self.consonant_len + sample[1] * self.consonant_len + sample[2]
        except IndexError:
            print("Bad shape of sample")
            return None

    def get_uniq_labels(self, arr: np.array) -> np.ndarray:
        encoded_list = self._create_ohe(arr)
        row_sum = np.sum(encoded_list, axis=0)
        uniq_labels = np.where(row_sum == 0)[0]
        return uniq_labels
