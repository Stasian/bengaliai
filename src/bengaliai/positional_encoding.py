import numpy as np


class TwoMatrixPositionalEncoding:
    """Create two positional encoding mask"""

    def __init__(self, height: int, width: int):
        self._height, self._width = height, width
        self._f_mask = self._create_first_mask()
        self._s_mask = self._create_second_mask()

    def _create_first_mask(self) -> np.array:
        self._f_mask = np.full((self._height, self._width), np.linspace(-1, 1, self._width))
        return self._f_mask

    def _create_second_mask(self) -> np.array:
        self._s_mask = np.full((self._width, self._height), np.linspace(-1, 1, self._height)).T
        return self._s_mask

    def get_first_mask(self) -> np.array:
        return self._f_mask

    def get_second_mask(self) -> np.array:
        return self._s_mask
