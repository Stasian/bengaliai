from typing import List, Iterable
from abc import abstractmethod
import gc
import imageio

from .data_utils import read_parquets

import numpy as np
import pandas as pd
import torch
from torch.utils.data import Dataset
from torch.utils.data.sampler import WeightedRandomSampler


class BaseDataset(Dataset):
    """Base dataset interface"""

    def __init__(self, data: pd.DataFrame, labels: bool, transform=None, transform_y=None):
        self.data = data
        self.transform = transform
        self.transform_y = transform_y
        self.labels = labels

    def __len__(self) -> int:
        return len(self.data)

    @abstractmethod
    def get_x(self, idx: int):
        pass

    @abstractmethod
    def get_y(self, idx: int):
        pass

    def __getitem__(self, idx: int):
        """ Get idx element of the dataset

        Arguments:
                idx {int} -- index of element in dataset
        Returns:
                dict -- element of the dataset
                {
                        "data": data,
                        "labels": labels
                }
        """
        x = self.get_x(idx)
        if self.transform is not None:
            item = self.transform(image=x)

        if self.labels:
            y = self.get_y(idx)
            if self.transform_y is not None:
                y = self.transform_y(y)
            y = torch.from_numpy(y)
            item["labels"] = y
        return item


class BengaliaiDataset(BaseDataset):
    def __init__(
        self,
        labels: bool,
        parquet_path: str,
        using_idxs=None,
        target_path: str = None,
        transform=None,
        transform_y=None,
        save_mem=False,
        mem_name=None,
    ):
        self.width = 236
        self.height = 137

        x_data = self.load_x(parquet_path, using_idxs)
        if save_mem:
            self.save_mem(x_data, mem_name)

        super().__init__(data=x_data, labels=labels, transform=transform, transform_y=transform_y)
        if labels:
            targets = self.load_df(target_path)
            targets = targets[using_idxs]
            targets = targets.astype(int, copy=False)
            self.y_df = targets
        gc.collect()

    def load_x(self, parquet_path, idx=None):
        x = read_parquets(parquet_path)
        if idx is not None:
            x = x[idx]
        return x

    def save_mem(self, arr, path):
        fp = np.memmap(path, dtype=arr.dtype, mode="w+", shape=arr.shape)
        fp[:] = arr[:]

    def load_df(self, path: str):
        df = pd.read_csv(path)
        df = df.iloc[:, [1, 2, 3]].values
        return df

    def get_x(self, idx: int):
        """ Get data sample
        """
        x = self.data[idx]
        x = x.reshape(self.height, self.width, 1)
        x = (255 - x).astype(np.float32) / 255.0
        return x

    def get_y(self, idx: int):
        """ Get sample labels
        """
        graph_root = np.zeros(168)  # first binary feature
        vowel_diac = np.zeros(11)  # second binary feature
        conson_diac = np.zeros(7)  # third binary feature

        graph_val, vowel_val, conson_val = self.y_df[idx, [0, 1, 2]]

        if graph_val <= 168 and graph_val >= 0:
            graph_root[graph_val] = 1
        if vowel_val <= 11 and vowel_val >= 0:
            vowel_diac[vowel_val] = 1
        if conson_val <= 7 and conson_val >= 0:
            conson_diac[conson_val] = 1
        y = np.concatenate((graph_root, vowel_diac, conson_diac))
        return y.astype(np.float32)

    def get_mean(self):
        return np.mean(self.data)

    def get_std(self):
        return np.std(self.data)


class BengaliaiDatasetMemmap(BengaliaiDataset):
    def load_x(self, parquet_path, idx):
        new_shape = (-1, self.height, self.width, 1)
        fpr = np.memmap(parquet_path, dtype="uint8", mode="r")
        return fpr.reshape(new_shape)


class MatriVasha(BengaliaiDataset):
    def __init__(
        self, labels, parquet_path, using_idxs=None, target_path=None, transform=None, save_mem=False, mem_name=None
    ):
        super().__init__(
            labels,
            parquet_path,
            using_idxs=using_idxs,
            target_path=target_path,
            transform=transform,
            save_mem=save_mem,
            mem_name=mem_name,
        )
        self.width = 236
        self.height = 137

        x_data = self.load_x(parquet_path, using_idxs)
        if save_mem:
            self.save_mem(x_data, mem_name)
        gc.collect()

    def load_x(self, parquet_path, idx=None):
        x = pd.read_csv(parquet_path)
        x = x.iloc[:, 1:].values
        if idx is not None:
            x = x[idx]
        return x


def get_weghted_sampler(train, any_weight, other_weight, len_factor=1):
    any_classes = train["any"] == 1
    notany = train["any"] == 0
    sampler_weigts = np.zeros(len(train))
    sampler_weigts[any_classes] = any_weight
    sampler_weigts[notany] = other_weight

    return WeightedRandomSampler(sampler_weigts, int(len(sampler_weigts) * len_factor))
