from typing import Tuple


class StagingScheduler:
    def __init__(self, schedule: Tuple[int]):
        self.schedule_precheck(schedule)
        self.schedule = schedule

    def on_epoch_start(self, epoch):
        cnt = 0
        stage = 0
        for i, epoches in enumerate(self.schedule):
            cnt += epoches
            if cnt >= epoch:
                break
            else:
                stage = i
        return stage

    def schedule_precheck(self, schdule):
        pass
