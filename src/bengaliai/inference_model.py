import torch
import gc

from torch import nn
from tqdm import tqdm


class InferenceNet(nn.Module):
    def __init__(self, model, use_sigmoid=True):
        super().__init__()
        self._model = model
        if use_sigmoid:
            self._use_sigmoid = True
            self._sigmoid = nn.Sigmoid()
        self._model.eval()

    def _setup_defaults(self):
        self._preds = []

    def forward(self, image: torch.Tensor) -> torch.Tensor:
        preds = self._model(image)
        if self._use_sigmoid:
            preds = self._sigmoid(preds)
        return preds

    def _get_batch(self, batch: dict) -> torch.Tensor:
        x = batch["image"]
        return x.cuda()

    def _aggregate_preds(self, preds):
        preds = torch.cat(preds, 0)
        return preds

    def predict(self, dataloader):
        self._setup_defaults()
        with torch.no_grad():
            for batch in tqdm(dataloader):
                x = self._get_batch(batch)
                preds = self.forward(x)
                preds = preds.detach().cpu()
                self._preds.append(preds)

        res = self._aggregate_preds(self._preds)
        return res.numpy()
