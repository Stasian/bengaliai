from abc import ABC, abstractmethod
import numpy as np
import sklearn.metrics
from bengaliai.util import convert_to_categorical


class Callback(ABC):
    """Use this abstraction for injecting nctionality in training process"""

    def on_training_step(self, model, outs_dict):
        return outs_dict

    def on_training_end(self, model, outs_list, outs_dict):
        return outs_list, outs_dict

    def on_validation_step(self, model, outs_dict):
        return outs_dict

    def on_validation_end(self, model, outs_list, outs_dict):
        return outs_list, outs_dict

    def on_test_step(self, model, outs_dict):
        return outs_dict

    def on_test_end(self, model, outs_list, outs_dict):
        return outs_list, outs_dict


class LossLoggerCallback(Callback):
    def __init__(self):
        self.setup_defaults()

    def setup_defaults(self):
        self.cnt = 0
        self.loss_sum = 0

    def on_training_step(self, model, outs_dict):
        batch_len, loss = outs_dict["x"].shape[0], outs_dict["loss"]
        self.cnt += batch_len
        self.loss_sum += loss
        model.logger.experiment.log({"mean_loss": loss / batch_len})
        return outs_dict

    def on_training_end(self, model, outs_list, outs_dict):
        model.logger.experiment.log({"mean_epoch_loss": self.loss_sum / self.cnt})
        self.setup_defaults()
        return outs_list, outs_dict


class CleanupCallback(Callback):
    """
    Use this callback to define, what values you want to keep after every step
    """

    def __init__(self, train_step=[], train_end=[], validation_step=[], validation_end=[], test_step=[], test_end=[]):
        self.train_step = set(train_step)
        self.train_end = set(train_end)
        self.validation_step = set(validation_step)
        self.validation_end = set(validation_end)
        self.test_step = set(test_step)
        self.test_end = set(test_end)

    def _remove(self, outs_dict, keep_values):
        for key in list(outs_dict.keys()):
            if key not in keep_values:
                outs_dict.pop(key)
        return outs_dict

    def on_training_step(self, model, outs_dict):
        outs_dict = self._remove(outs_dict, self.train_step)
        return outs_dict

    def on_training_end(self, model, outs_list, outs_dict):
        outs_dict = self._remove(outs_dict, self.train_end)
        return outs_list, outs_dict

    def on_validation_step(self, model, outs_dict):
        outs_dict = self._remove(outs_dict, self.validation_step)
        return outs_dict

    def on_validation_end(self, model, outs_list, outs_dict):
        outs_dict = self._remove(outs_dict, self.validation_end)
        return outs_list, outs_dict

    def on_test_step(self, model, outs_dict):
        outs_dict = self._remove(outs_dict, self.test_step)
        return outs_dict

    def on_test_end(self, model, outs_list, outs_dict):
        outs_dict = self._remove(outs_dict, self.test_end)
        return outs_list, outs_dict


class PredictCallback(Callback):
    def __init__(self):
        self.predicts = []
        self.answers = []

    def on_test_step(self, model, outs_dict):
        self.predicts.append(outs_dict["y_hat"].detach().cpu().numpy())
        self.predicts.append(outs_dict["y"].detach().cpu().numpy())
        return outs_dict

    def on_test_end(self, model, outs_list, outs_dict):
        all_preds = np.vstack(self.predicts)
        all_ans = np.vstack(self.answers)
        # model.set_predict_matrix(all_preds)
        model.set_truth_matrix(all_ans)
        print(all_preds)
        return outs_list, outs_dict


class MetricsCallback(Callback):
    def __init__(self):
        self.predicts = []
        self.answers = []

    def on_validation_step(self, model, outs_dict):
        self.predicts.append(outs_dict["y_hat"])
        self.answers.append(outs_dict["y"])
        return outs_dict

    def on_validation_end(self, model, outs_list, outs_dict):
        preds = [tensor.detach().cpu().numpy() for tensor in self.predicts]
        ans = [tensor.detach().cpu().numpy() for tensor in self.answers]
        if len(ans) == 5:
            sanity_check = True
        else:
            sanity_check = False

        pred_matrix = np.vstack(preds)
        ans_matrix = np.vstack(ans)
        cat_pred_matrix = convert_to_categorical(pred_matrix)
        cat_ans_matrix = convert_to_categorical(ans_matrix)
        model.set_predict_matrix(pred_matrix)
        model.set_truth_matrix(ans_matrix)
        # print('hell = ', model.get_predict_matrix())
        # print('suka =', pred_matrix.shape)
        scores = []
        for component in range(3):
            y_true_subset = cat_ans_matrix[:, component]
            y_pred_subset = cat_pred_matrix[:, component]
            scores.append(sklearn.metrics.recall_score(y_true_subset, y_pred_subset, average="macro"))
        final_score = np.average(scores, weights=[2, 1, 1])
        if sanity_check:
            final_score = 0

        outs_dict["val_score"] = final_score
        outs_dict["progress_bar"] = {"val_score": final_score}
        model.logger.experiment.log({"val_score": final_score})
        # print('\nour score is :', final_score, '\n')
        return outs_list, outs_dict


class ComputeValLoss(Callback):  # probably move to loss logger
    def __init__(self):
        self.cnt = 0
        self.loss_sum = 0

    def on_validation_step(self, model, outs_dict):
        loss = model._eval_loss(outs_dict["y_hat"], outs_dict["y"])  # think
        self.loss_sum += loss
        self.cnt += outs_dict["y"].shape[0]
        return outs_dict

    def on_validation_end(self, model, outs_list, outs_dict):
        outs_dict["val_loss"] = self.loss_sum / self.cnt
        model.logger.experiment.log({"val_loss": self.loss_sum / self.cnt})

        self.cnt = 0
        self.loss_sum = 0
        return outs_list, outs_dict
