from abc import ABC, abstractmethod

import torch
import numpy as np


class BaseMix(ABC):
    def __init__(self, beta=1):
        self.beta = beta
        self.clean_up()

    @abstractmethod
    def process(self, sample: dict):
        pass

    def get_res(self):
        return self.res

    def clean_up(self):
        self.res = []

    def mix(self, lambda_, seq):
        weights = torch.Tensor([lambda_, 1 - lambda_])  ## TODO
        samples = torch.stack(seq)
        samples_shape = samples.shape
        if len(samples_shape) == 2:
            weights = weights[:, None]
        elif len(samples_shape) == 4:
            weights = weights[:, None, None, None]

        weighted_sample = torch.sum(samples * weights, axis=0)
        return weighted_sample

    def reset_state(self):
        self.imgs = []
        self.labels = []


class Mixup(BaseMix):
    """https://arxiv.org/abs/1710.09412, but with uniform distribution"""

    def __init__(self, beta=1):
        super().__init__(beta=beta)
        self.reset_state()
        self.mix_samples = 2

    def process(self, sample: dict) -> None:
        image, labels = sample["image"], sample["labels"]

        self.imgs.append(image)
        self.labels.append(labels)

        if len(self.imgs) == self.mix_samples:
            lambda_ = np.random.beta(self.beta, self.beta)
            img, labels = self.mix(lambda_, self.imgs), self.mix(lambda_, self.labels)
            self.res.append({"image": img, "labels": labels})
            self.reset_state()


class Cutmix(BaseMix):
    """Works only with 2 images"""

    def __init__(self, beta=1):
        super().__init__(beta=beta)
        self.reset_state()

    def process(self, sample: dict) -> None:
        image, labels = sample["image"], sample["labels"]
        lambda_ = np.random.beta(self.beta, self.beta)
        self.imgs.append(image)
        self.labels.append(labels)

        if len(self.imgs) == 2:
            img, labels = self.mix_images(lambda_, self.imgs), self.mix(lambda_, self.labels)
            self.res.append({"image": img, "labels": labels})
            self.reset_state()

    def mix_images(self, lambda_, imgs) -> np.array:
        img, img2 = imgs

        bbx1, bby1, bbx2, bby2 = self.rand_bbox(img.shape, lambda_)
        new_img = np.copy(img)
        new_img[bbx1:bbx2, bby1:bby2] = img2[bbx1:bbx2, bby1:bby2]
        new_img = torch.from_numpy(new_img)

        return new_img

    def rand_bbox(self, size: np.array, lambda_: float) -> set:
        if len(size) == 3:
            W = size[1]
            H = size[2]
        elif len(size) == 2:
            W = size[0]
            H = size[1]
        else:
            raise Exception

        cut_rat = np.sqrt(1.0 - lambda_)
        cut_w = np.int(W * cut_rat)
        cut_h = np.int(H * cut_rat)

        # uniform
        cx = np.random.randint(W)
        cy = np.random.randint(H)

        bbx1 = np.clip(cx - cut_w // 2, 0, W)
        bby1 = np.clip(cy - cut_h // 2, 0, H)
        bbx2 = np.clip(cx + cut_w // 2, 0, W)
        bby2 = np.clip(cy + cut_h // 2, 0, H)

        return bbx1, bby1, bbx2, bby2


class CutmixDoubleLoss(Cutmix):
    """Works only with 2 images"""

    def __init__(self):
        super().__init__()
        self.reset_state()

    def set_lambda(self, lambda_):
        self.lambda_ = lambda_

    def process(self, sample: dict) -> None:
        image, labels = sample["image"], sample["labels"]
        self.imgs.append(image)
        self.labels.append(labels)

        if len(self.imgs) == 2:
            img = self.mix_images(self.lambda_, self.imgs)
            self.res.append({"image": img, "labels": self.labels})
            self.reset_state()


class MixupDoubleLoss(Mixup):
    """Works only with 2 images"""

    def __init__(self):
        super().__init__()
        self.reset_state()

    def set_lambda(self, lambda_):
        self.lambda_ = lambda_

    def process(self, sample: dict) -> None:
        image, labels = sample["image"], sample["labels"]
        self.imgs.append(image)
        self.labels.append(labels)

        if len(self.imgs) == 2:
            img = self.mix(self.lambda_, self.imgs)
            self.res.append({"image": img, "labels": self.labels})
            self.reset_state()


class PassMix(BaseMix):
    """Do nothing"""

    def process(self, sample: dict) -> None:
        self.res.append(sample)


class MixCollate:
    def __init__(self, mix_classes=[], values=[], double_loss=False, beta=1):
        """Apply mixup or cutmix to batch"""
        self.mix_classes = mix_classes
        self.values = values
        self.double_loss = double_loss
        self.beta = beta

    def __call__(self, batch: dict) -> dict:
        images = [i["image"] for i in batch]
        labels = [i["labels"] for i in batch]
        if self.double_loss:
            lambda_ = np.random.beta(self.beta, self.beta)
        i, res = 0, []
        for mix_class, value in zip(self.mix_classes, self.values):
            mix_class.clean_up()
            if self.double_loss:
                mix_class.set_lambda(lambda_)

            for _ in range(value):
                sample = {"image": images[i], "labels": labels[i]}
                mix_class.process(sample)
                i += 1

            res.extend(mix_class.get_res())

        images = torch.stack([i["image"] for i in res])
        if self.double_loss:
            labels = [torch.stack([i["labels"][j] for i in res]) for j in range(2)]
        else:
            labels = torch.stack([i["labels"] for i in res])

        result_batch = {"image": images, "labels": labels}
        if self.double_loss:
            result_batch["lambda_"] = lambda_
        return result_batch
