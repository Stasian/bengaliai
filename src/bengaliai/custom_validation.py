import numpy as np
from bengaliai.make_ohe import Ohe


class CustomValidation:
    """Picks random classes(equals to n_splits) and puts one of them in every y split"""

    def __init__(self, n_splits=5, shuffle=False, seed=0, n_out=1):
        self.seed = seed
        self.n_splits = n_splits
        self.n_out = n_out

        self.shuffle = shuffle
        self.class_map_path = "/data/class_map.csv"

    def split(self, X: np.array, y: np.array) -> set:
        np.random.seed(self.seed)
        all_out = self.n_out * self.n_splits

        ohe = Ohe(self.class_map_path, all_comb=False)
        ohe_labels = ohe(y.values)

        num_samples = ohe_labels.shape[0]
        y_size = int((1 / self.n_splits) * num_samples)
        if y_size < self.n_out:
            raise ValueError("cant take out more classes than fold size")

        samples_idxs = np.arange(num_samples)
        special_labels = np.random.choice(ohe_labels.shape[1], all_out, replace=False).reshape(self.n_splits, -1)
        special_labels_idxs = []
        #       Выбираем рандомные классов
        already_used = np.array([], dtype="int32")
        #       отдельный array для индексов элементов, в нем просто все выбранные эл-ты
        for split in special_labels:
            fold_labels = np.array([], dtype="int32")
            for label in split:
                certain_label = np.argwhere(ohe_labels[:, label] == 1)[0]
                fold_labels = np.append(fold_labels, certain_label)
                already_used = np.concatenate((already_used, certain_label))
            special_labels_idxs.append(fold_labels)
        # выбираем индексы элементов по классов в отдельные array

        y_folds = np.array([], dtype="int32")

        for fold in range(self.n_splits):
            current_y_size = special_labels_idxs[fold].size
            need_to_add = y_size - current_y_size
            current_y_samples = special_labels_idxs[fold]
            # узнаем сколько нужно добавить и какие эл-те в фолде уже есть
            left_classes = np.delete(samples_idxs, already_used)
            # будем выбирать из тех эл-тов, которые не использовались
            if self.shuffle:
                left_classes_samples = np.random.choice(left_classes, need_to_add, replace=False)
            # добавляем рандомно из оставшихся
            else:
                left_classes_samples = left_classes[:need_to_add]
            # добавляем последовательно оставшиеся
            already_used = np.concatenate([already_used, left_classes_samples])
            # добавляем к уже использованным
            current_fold = np.hstack((current_y_samples, left_classes_samples))
            if self.shuffle:
                np.random.shuffle(current_fold)
            y_folds = np.concatenate([y_folds, current_fold])

        y_folds = y_folds.reshape(self.n_splits, -1)

        X_folds = np.array([], dtype="int32")
        for fold in range(self.n_splits):
            current_fold = np.delete(samples_idxs, y_folds[fold])
            if self.shuffle:
                np.random.shuffle(current_fold)
            X_folds = np.concatenate([X_folds, current_fold])

        X_folds = X_folds.reshape(self.n_splits, -1)
        return X_folds, y_folds
