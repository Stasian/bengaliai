import argparse
import os
import sys
import gc
from importlib import import_module

import pandas as pd
import numpy as np

LOCAL = False
if not LOCAL:
    sys.path.append("../input/newdata2")

from bengaliai.data_utils import read_parquets
from bengaliai.util import convert_to_categorical, make_submission, instance_anything, debug_parquet_multiplier
from bengaliai.inference_model import InferenceNet

import torch


def load_model(model, path_to_checkpoint):
    checkpoint = torch.load(path_to_checkpoint, map_location=lambda storage, loc: storage)
    model.load_state_dict(checkpoint["state_dict"])
    model.to("cuda")
    return model


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="run config")
    parser.add_argument("--config", type=str, help="path to module", default="predict_config")
    if not LOCAL:
        config_module = "predict_config"
    else:
        args = parser.parse_args()
        config_module = args.config

    config_module = import_module(config_module)  # load config as python module
    config = config_module.config

    # df = pd.read_csv(config["data"]["test_df_path"], encoding="utf")

    # parquets = debug_parquet_multiplier(parquets

    path = "/data_fast/data/bengaliai/matrivasha_feamale1.csv"

    model = InferenceNet(instance_anything(config["backbone"]))
    path_to_checkpoint = config["path_to_checkpoint"]
    model = load_model(model, path_to_checkpoint)

    config["stages"][1]["test"]["dataset_constructor"]["kwargs"]["parquet_path"] = [path]
    config_data = config["stages"][1]["test"]
    dataset = instance_anything(config_data["dataset_constructor"])
    config_data["dataloader_constructor"]["kwargs"]["dataset"] = dataset
    dataloader = instance_anything(config_data["dataloader_constructor"])
    print("MAKING PREDICT!")
    res = model.predict(dataloader)
    # res = convert_to_categorical(res)
    print("WORKING WITH PREDICT MATRIX!")
    # DANGER Not even launched once, do not use while convert_to_categorical TODO not finished!
    graph_col, vowel_col, conson_col = convert_to_categorical(res, origin_dataset=False)

    k = res.shape[0]
    image_id = []
    for i in range(k):
        image_id.append("Test_" + str(i))
    print("CREATING CSV WITH ANSWERS!")
    df = pd.DataFrame()
    df["image_id"] = image_id
    df["grapheme_root"] = graph_col
    df["vowel_diacritic"] = vowel_col
    df["consonant_diacritic"] = conson_col
    df.to_csv("pseudo_label_matrivasha.csv", index=False)
    print("CSV IS DONE!")
