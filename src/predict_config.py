import os

from bengaliai.dataset import BengaliaiDataset
from bengaliai.transforms import OneToThree
from bengaliai.callbacks import CleanupCallback, ComputeValLoss, PredictCallback
from bengaliai.backbones import SeResnext50Backbone, EfficientnetB3ns, SeResnext101Backbone

import torch
from torch.optim.lr_scheduler import ReduceLROnPlateau
from torch.optim import Adam
from torch.utils.data import DataLoader
import numpy as np
from albumentations import (
    CLAHE,
    RandomRotate90,
    Transpose,
    ShiftScaleRotate,
    Blur,
    OpticalDistortion,
    GridDistortion,
    Compose,
    Resize,
    Normalize,
    HorizontalFlip,
    RandomBrightnessContrast,
    RandomResizedCrop,
    VerticalFlip,
    Rotate,
)
from albumentations.pytorch import ToTensorV2


BATCH_SIZE = 100

augs_224 = Compose([Resize(224, 224), OneToThree(), ToTensorV2()])

data_config = {
    "data": {"test_df_path": "../input/bengaliai-cv19/test.csv"},
    "stages": [
        # 0 stage
        {
            "test": {
                "dataset_constructor": {
                    "base_class": BengaliaiDataset,
                    "kwargs": {"labels": False, "transform": augs_224},
                },
                "dataloader_constructor": {
                    "base_class": DataLoader,
                    "kwargs": {"num_workers": 2, "pin_memory": True, "batch_size": BATCH_SIZE, "shuffle": False},
                },
            }
        }
    ],
}

configs = [
    # {
    #     "backbone": {"base_class": EfficientnetB3ns(), "kwargs": {"out": 186, "pretrained": False, "drop_rate": 0.5}},
    #     "path_to_checkpoint": "/src/workspace/models/efficient_b3a_cutmix_secondstage_midaugs_three_pure6824290551730287196_fold_0/_ckpt_epoch_1.ckpt",
    # },
    # {
    #     "backbone": {
    #         "base_class": SeResnext50Backbone(),
    #         "kwargs": {"out": 186, "pretrained": None, "avg_pool_size": 7},
    #     },
    #     "path_to_checkpoint": "/src/workspace/models/seresnext_50_warmup_newval_50_pure_from-2089770556746614326_fold_0/_ckpt_epoch_3.ckpt",
    # },
    {
        "path_to_checkpoint": "/src/workspace/models/_ckpt_epoch_3.ckpt",
        "backbone": {
            "base_class": SeResnext101Backbone(),
            "kwargs": {"out": 186, "pretrained": None, "avg_pool_size": 7},
        },
    }
]
