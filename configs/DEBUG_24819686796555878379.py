import os

from bengaliai.dataset import BengaliaiDataset
from bengaliai.transforms import OneToThree
from bengaliai.callbacks import CleanupCallback, ComputeValLoss, PredictCallback

import torch
from torch.optim.lr_scheduler import ReduceLROnPlateau
from torch.optim import Adam
from torch.utils.data import DataLoader
import numpy as np
from albumentations import (
    CLAHE,
    RandomRotate90,
    Transpose,
    ShiftScaleRotate,
    Blur,
    OpticalDistortion,
    GridDistortion,
    Compose,
    Resize,
    Normalize,
    HorizontalFlip,
    RandomBrightnessContrast,
    RandomResizedCrop,
    VerticalFlip,
    Rotate,
)
from albumentations.pytorch import ToTensorV2


name = "DEBUG_2"
BATCH_SIZE = 20


def get_model(out):
    model = torch.hub.load("facebookresearch/WSL-Images", "resnext101_32x8d_wsl")
    model.fc = torch.nn.Linear(2048, out)
    return model


augs = Compose([OneToThree(), Resize(224, 224), ToTensorV2()])

callbacks = (
    ComputeValLoss(),
    PredictCallback(),
    CleanupCallback(train_step=["loss"], validation_end=["mean_val_loss"]),
)

config = {
    "name": name,
    "callbacks": callbacks,
    "data": {
        "train_df_path": "/data/train.csv",
        "train_parquets": (
            "/data/train_image_data_0.parquet",
            "/data/train_image_data_1.parquet",
            "/data/train_image_data_2.parquet",
            "/data/train_image_data_3.parquet",
        ),
    },
    "debug": {"df_num_samples": 1000},
    "cv": {"generator_kwargs": {"n_splits": 5, "random_state": 0}, "using_splits": 2},
    "backbone": {"base_class": get_model, "kwargs": {"out": 186}},
    "opter_constructor": {"base_class": Adam, "kwargs": {"lr": 1e-4}},
    "loss": {"base_class": torch.nn.BCELoss},
    "lightning_callbacks": {
        "checkpoint_path": "/src/workspace/models",
        "ModelCheckpoint_kwargs": {
            "save_best_only": 1,
            "verbose": True,
            "monitor": "mean_val_loss",
            "mode": "min",
            "prefix": "",
        },
        "EarlyStopping_kwargs": {
            "monitor": "mean_val_loss",
            "min_delta": 0.00,
            "patience": 2,
            "verbose": True,
            "mode": "min",
        },
        "logger_base_dir": "/src/workspace/exp",
    },
    "stages": [
        # 0 stage
        {
            "train": {
                "dataset_constructor": {"base_class": BengaliaiDataset, "kwargs": {"labels": True, "transform": augs}},
                "dataloader_constructor": {
                    "base_class": DataLoader,
                    "kwargs": {"num_workers": 32, "pin_memory": True, "batch_size": BATCH_SIZE, "shuffle": True},
                },
            },
            "val": {
                "dataset_constructor": {"base_class": BengaliaiDataset, "kwargs": {"labels": True, "transform": augs}},
                "dataloader_constructor": {
                    "base_class": DataLoader,
                    "kwargs": {"num_workers": 32, "pin_memory": True, "batch_size": BATCH_SIZE, "shuffle": False},
                },
            },
            "test": {
                "dataset_constructor": {"base_class": BengaliaiDataset, "kwargs": {"labels": True, "transform": augs}},
                "dataloader_constructor": {
                    "base_class": DataLoader,
                    "kwargs": {"num_workers": 32, "pin_memory": True, "batch_size": BATCH_SIZE, "shuffle": False},
                },
            },
        }
    ],
    "trainer": {
        "max_nb_epochs": 5,
        "use_amp": False,
        "gpus": [2],
        # "distributed_backend": "dp",
        # "train_percent_check": 1,
        # "log_gpu_memory": "all",
        # "fast_dev_run": True,
    },
}
